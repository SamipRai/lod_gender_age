from flask import Flask, jsonify, request
import random
import time
import keras
from tensorflow.keras.models import load_model
from keras.utils import CustomObjectScope
from keras.initializers import glorot_uniform
from keras.preprocessing import image
from PIL import Image
import numpy as np
import cv2
import os





def detect():
    model = load_model('my_age_model.h5') 
    for i in os.listdir("DB"):
        for j in os.listdir("DB/"+i):
            print("DB/"+i+"/"+j)
            try:
                img = cv2.imread("DB/"+i+"/"+j)
                test_image = image.img_to_array(img)
                test_image = np.expand_dims(test_image, axis = 0)
                result = model.predict(test_image)
                print(result)
                file = open("DB/"+i+"/"+"age.txt", "a+") 
                file.write("27-37\n" if result[0][0]<0.5 else "38-53\n") 
                file.close()
            except:
                print("ok")

         
